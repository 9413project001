#include <stdio.h>
#include <string.h>
#include "genpwd.h"

const char ch[] = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

char(*genpwd(const char* startstr))[16]
{
	// length of password
	//static int length = 5;

	static char (*arrpwd)[16] = NULL;
	static int arrindex[15] = {0};
	arrindex[PASSWORD_LENGTH-1] = -1;
	
	int chlen = strlen(ch);
	if(startstr != NULL && strlen(startstr) == PASSWORD_LENGTH) {
		for(int i=0;i<PASSWORD_LENGTH;i++) {
			for(int j=0;j<chlen;j++) {
				if(startstr[i]==ch[j]) {arrindex[i] = j;break;}
			}
		}
	}

	if(arrpwd == NULL) {
		arrpwd = new char[100000][16];
		memset(arrpwd,0,100000*16);
	} else {
		memset(arrpwd,0,100000*16);
	}

	static int breakflg = 0;

	if(breakflg==1) {
		delete[] arrpwd;
		return NULL;
	}

	for(int cur=0;cur<100000 && breakflg==0;cur++) {
		for(;;){
			if (breakflg) goto RETURN;
			//add
			for(int i=PASSWORD_LENGTH-1,plus=1;i>=0;i--) {
				if(plus==1) {
					if(++arrindex[i] == chlen) {
						arrindex[i] = 0;
						if (i==0) breakflg=1;
					} else {
						break;
					}
				}
			}
			//check
			int upperflag = 0, numflag = 0;
			for(int i=0;i<PASSWORD_LENGTH;i++) {
				if(arrindex[i]>=26 && arrindex[i]<=51) upperflag = 1;
				if(arrindex[i]>=52 && arrindex[i]<=61) numflag = 1;
			}
			if(upperflag && numflag) break;
		}

		//output
		for(int i=PASSWORD_LENGTH-1,plus=1;i>=0;i--) {
			arrpwd[cur][i] = ch[arrindex[i]];
		}
		arrpwd[cur][PASSWORD_LENGTH] = '\0';
	}
RETURN:
	return arrpwd;
}
