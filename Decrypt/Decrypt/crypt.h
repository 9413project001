
#pragma once

#define LIB_GPU 1
#define LIB_CPU 2

#define MD5_LIB LIB_GPU

#ifdef LIB_GPU
#define MD5_EXPORT inline
#else
#define MD5_EXPORT 
#endif

#if MD5_LIB == LIB_CPU
#define MD5_FUNC_RESTRICT restrict(cpu)
#endif

#if MD5_LIB == LIB_GPU
#define MD5_FUNC_RESTRICT restrict(amp,cpu)
#endif

MD5_EXPORT extern void __md5_crypt_r (const void *key, unsigned int keylen, const void *salt, unsigned int *buffer) MD5_FUNC_RESTRICT;