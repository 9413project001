#include <stdlib.h>
#include <string.h>
#include "md5.h"
#include "../crypt.h"
#define MAX(a,b) a>b?a:b
#define MIN(a,b) a<b?a:b

static char* buffer;
static const char md5_salt_prefix[] = "$1$";

void b64_from_24bit (CHAR*&cp, int buflen, unsigned int b2, unsigned int b1, unsigned int b0, int n) ON_GPU
{
	static const CHAR b64t[65] = "./0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
	unsigned int w = (b2 << 16) | (b1 << 8) | b0;
	while (n-- > 0 && buflen > 0)
	{
		*cp++ = b64t[w & 0x3f];
		--buflen;
		w >>= 6;
	}
}

char * __md5_crypt_r (const char *key, const char *salt, char *buffer, int buflen) ON_GPU
{
	unsigned char alt_result[16];
	size_t salt_len;
	size_t key_len;
	size_t cnt;
	char *cp;
	char *copied_key = NULL;
	char *copied_salt = NULL;
	char *free_key = NULL;
	size_t alloca_used = 0;

	salt_len = strcspn (salt, "$")<8?strcspn (salt, "$"):8;
	key_len = strlen (key);

	if ((key - (char *) 0) % __alignof (md5_uint32) != 0)
	{
		char *tmp;

		free_key = tmp = (char *) malloc (key_len + __alignof (md5_uint32));
		if (tmp == NULL)
		{
			return NULL;
		}

		key = copied_key =
			(char*)memcpy (tmp + __alignof (md5_uint32)
			- (tmp - (char *) 0) % __alignof (md5_uint32),
			key, key_len);
	}

	if ((salt - (char *) 0) % __alignof (md5_uint32) != 0)
	{
		char *tmp = (char *) malloc (salt_len + __alignof (md5_uint32));
		salt = copied_salt =
			(char*)memcpy (tmp + __alignof (md5_uint32)
			- (tmp - (char *) 0) % __alignof (md5_uint32),
			salt, salt_len);
	}

	struct md5_ctx ctx;
	struct md5_ctx alt_ctx;

	/* Prepare for the real work.  */
	md5_init_ctx (&ctx);

	/* Add the key string.  */
	md5_process_bytes (key, key_len, &ctx);

	/* Because the SALT argument need not always have the salt prefix we
	add it separately.  */
	md5_process_bytes (md5_salt_prefix, sizeof (md5_salt_prefix) - 1,
		&ctx);

	/* The last part is the salt string.  This must be at most 8
	characters and it ends at the first `$' character (for
	compatibility with existing implementations).  */
	md5_process_bytes (salt, salt_len, &ctx);


	/* Compute alternate MD5 sum with input KEY, SALT, and KEY.  The
	final result will be added to the first context.  */
	md5_init_ctx (&alt_ctx);

	/* Add key.  */
	md5_process_bytes (key, key_len, &alt_ctx);

	/* Add salt.  */
	md5_process_bytes (salt, salt_len, &alt_ctx);

	/* Add key again.  */
	md5_process_bytes (key, key_len, &alt_ctx);

	/* Now get result of this (16 bytes) and add it to the other
	context.  */
	md5_finish_ctx (&alt_ctx, alt_result);

	/* Add for any character in the key one byte of the alternate sum.  */
	for (cnt = key_len; cnt > 16; cnt -= 16)
		md5_process_bytes (alt_result, 16, &ctx);
	md5_process_bytes (alt_result, cnt, &ctx);

	/* For the following code we need a NUL byte.  */
	*alt_result = '\0';

	/* The original implementation now does something weird: for every 1
	bit in the key the first 0 is added to the buffer, for every 0
	bit the first character of the key.  This does not seem to be
	what was intended but we have to follow this to be compatible.  */
	for (cnt = key_len; cnt > 0; cnt >>= 1)
		md5_process_bytes ((cnt & 1) != 0
		? (const void *) alt_result : (const void *) key, 1,
		&ctx);

	/* Create intermediate result.  */
	md5_finish_ctx (&ctx, alt_result);

	/* Now comes another weirdness.  In fear of password crackers here
	comes a quite long loop which just processes the output of the
	previous round again.  We cannot ignore this here.  */
	for (cnt = 0; cnt < 1000; ++cnt)
	{
		/* New context.  */
		md5_init_ctx (&ctx);

		/* Add key or last result.  */
		if ((cnt & 1) != 0)
			md5_process_bytes (key, key_len, &ctx);
		else
			md5_process_bytes (alt_result, 16, &ctx);

		/* Add salt for numbers not divisible by 3.  */
		if (cnt % 3 != 0)
			md5_process_bytes (salt, salt_len, &ctx);

		/* Add key for numbers not divisible by 7.  */
		if (cnt % 7 != 0)
			md5_process_bytes (key, key_len, &ctx);

		/* Add key or last result.  */
		if ((cnt & 1) != 0)
			md5_process_bytes (alt_result, 16, &ctx);
		else
			md5_process_bytes (key, key_len, &ctx);

		/* Create intermediate result.  */
		md5_finish_ctx (&ctx, alt_result);
	}

	/* Now we can construct the result string.  It consists of three
	parts.  */
	memcpy (buffer, md5_salt_prefix, MAX (0, buflen));
	cp = buffer;
	buflen -= sizeof (md5_salt_prefix) - 1;

	memcpy (cp, salt, MIN ((size_t) MAX (0, buflen), salt_len));
	cp = buffer;
	buflen -= MIN ((size_t) MAX (0, buflen), salt_len);

	if (buflen > 0)
	{
		*cp++ = '$';
		--buflen;
	}

	b64_from_24bit (cp, buflen, alt_result[0], alt_result[6], alt_result[12], 4);
	b64_from_24bit (cp, buflen, alt_result[1], alt_result[7], alt_result[13], 4);
	b64_from_24bit (cp, buflen, alt_result[2], alt_result[8], alt_result[14], 4);
	b64_from_24bit (cp, buflen, alt_result[3], alt_result[9], alt_result[15], 4);
	b64_from_24bit (cp, buflen, alt_result[4], alt_result[10], alt_result[5], 4);
	b64_from_24bit (cp, buflen, 0, 0, alt_result[11], 2);
	if (buflen <= 0)
	{
		buffer = NULL;
	}
	else
		*cp = '\0';		/* Terminate the string.  */

	/* Clear the buffer for the intermediate result so that people
	attaching to processes or reading core dumps cannot get any
	information.  We do it in this way to clear correct_words[]
	inside the MD5 implementation as well.  */
	md5_init_ctx (&ctx);
	md5_finish_ctx (&ctx, alt_result);
	memset (&ctx, '\0', sizeof (ctx));
	memset (&alt_ctx, '\0', sizeof (alt_ctx));

	if (copied_key != NULL)
		memset (copied_key, '\0', key_len);
	if (copied_salt != NULL)
		memset (copied_salt, '\0', salt_len);

	free (free_key);
	return buffer;
}

char * __md5_crypt (const char *key, const char *salt)
{
	/* We don't want to have an arbitrary limit in the size of the
	password.  We can compute the size of the result in advance and
	so we can prepare the buffer we pass to `md5_crypt_r'.  */
	static int buflen;
	int needed = 3 + strlen (salt) + 1 + 26 + 1;

	if (buflen < needed)
	{
		char *new_buffer = (char *) realloc (buffer, needed);
		if (new_buffer == NULL)
			return NULL;

		buffer = new_buffer;
		buflen = needed;
	}

	return __md5_crypt_r (key, salt, buffer, buflen);
}

char* crypt (const char *key, const char *salt)
{
	// only concern about crypted by md5
	return __md5_crypt (key, salt);
}