
#include "gpu_define.h"
#include "gpuStdLib.h"

using namespace gpuFunc;

unsigned int strcspn(const GPU_CHAR * _Str, const GPU_CHAR * _Control) ON_GPU
{
	for (int i = 0; _Str[i] != 0; i++)
	{
		for (int j = 0; _Control[j] != 0; j++)
		{
			if (_Str[i] == _Control[j])
			{
				return i;
			}
		}
	}
	return -1;
}

unsigned int strlen(const GPU_CHAR* str) ON_GPU
{
	int i;
	for (i = 0; str[i] != 0; i++);
	return i;
}

void memcpy(void* dst, const void* src, size_t size) ON_GPU
{
	GPU_CHAR* bufdst = (GPU_CHAR*)dst;
	GPU_CHAR* bufsrc = (GPU_CHAR*)src;
	for (int i = 0; i < size; i++)
	{
		bufdst = bufsrc;
	}
}

void memset(void* dst, int value, size_t size) ON_GPU
{
	size = size >> 2;
	GPU_CHAR* buf = (GPU_CHAR*)dst;
	for (int i = 0; i < size; i++)
	{
		buf[i] = value;
	}
}