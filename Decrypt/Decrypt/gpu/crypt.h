
#pragma once

#include "gpu_define.h"

extern CHAR * __md5_crypt_r (const CHAR *key, const CHAR *salt, CHAR *buffer, int buflen) ON_GPU;