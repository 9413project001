
#pragma once
#include "gpu_define.h"

namespace gpuFunc
{
	extern unsigned int strcspn(const GPU_CHAR * _Str, const GPU_CHAR * _Control) ON_GPU;

	extern unsigned int strlen(const GPU_CHAR* str) ON_GPU;

	extern void memcpy(void* dst, const void* src, size_t size) ON_GPU;

	extern void memset(void* dst, int value, size_t size) ON_GPU;
};