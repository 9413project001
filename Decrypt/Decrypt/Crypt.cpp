#include <stdlib.h>
#include <string.h>
#include "crypt.h"
#include "md5.h"

#define MAX(a,b) a>b?a:b
#define MIN(a,b) a<b?a:b


//void b64_from_24bit (char*&cp, int buflen, unsigned int b2, unsigned int b1, unsigned int b0, int n)
//{
//	static const char b64t[65] = "./0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
//	unsigned int w = (b2 << 16) | (b1 << 8) | b0;
//	while (n-- > 0 && buflen > 0)
//	{
//		*cp++ = b64t[w & 0x3f];
//		--buflen;
//		w >>= 6;
//	}
//}

MD5_EXPORT void __md5_crypt_r (const void *key, unsigned int keylen, const void *salt, unsigned int *alt_result) MD5_FUNC_RESTRICT
{
	size_t salt_len;
	size_t key_len;
	size_t cnt;
	size_t alloca_used = 0;

	salt_len = 7;
	key_len = keylen;

	struct md5_ctx ctx;
	struct md5_ctx alt_ctx;

	/* Prepare for the real work.  */
	md5_init_ctx (&ctx);

	/* Add the key string.  */
	md5_process_bytes (key, key_len, &ctx);

	/* Because the SALT argument need not always have the salt prefix we
	add it separately.  */
	int md5_salt_prefix = 0x243124;//"$1$"
	md5_process_bytes (&md5_salt_prefix, 3/*sizeof (md5_salt_prefix) - 1*/,&ctx);

	/* The last part is the salt string.  This must be at most 8
	characters and it ends at the first `$' character (for
	compatibility with existing implementations).  */
	md5_process_bytes (salt, salt_len, &ctx);


	/* Compute alternate MD5 sum with input KEY, SALT, and KEY.  The
	final result will be added to the first context.  */
	md5_init_ctx (&alt_ctx);

	/* Add key.  */
	md5_process_bytes (key, key_len, &alt_ctx);

	/* Add salt.  */
	md5_process_bytes (salt, salt_len, &alt_ctx);

	/* Add key again.  */
	md5_process_bytes (key, key_len, &alt_ctx);

	/* Now get result of this (16 bytes) and add it to the other
	context.  */
	md5_finish_ctx (&alt_ctx, alt_result);

	/* Add for any character in the key one byte of the alternate sum.  */
	for (cnt = key_len; cnt > 16; cnt -= 16)
		md5_process_bytes (alt_result, 16, &ctx);
	md5_process_bytes (alt_result, cnt, &ctx);

	/* For the following code we need a NUL byte.  */
	//*alt_result = '\0';

	/* The original implementation now does something weird: for every 1
	bit in the key the first 0 is added to the buffer, for every 0
	bit the first character of the key.  This does not seem to be
	what was intended but we have to follow this to be compatible.  */
	int i=0;
	for (cnt = key_len; cnt > 0; cnt >>= 1)
		md5_process_bytes ((cnt & 1) != 0
		? &i : (const void *) key, 1,
		&ctx);

	/* Create intermediate result.  */
	md5_finish_ctx (&ctx, alt_result);

	/* Now comes another weirdness.  In fear of password crackers here
	comes a quite long loop which just processes the output of the
	previous round again.  We cannot ignore this here.  */
	for (cnt = 0; cnt < 1000; ++cnt)
	{
		/* New context.  */
		md5_init_ctx (&ctx);

		/* Add key or last result.  */
		if ((cnt & 1) != 0)
			md5_process_bytes (key, key_len, &ctx);
		else
			md5_process_bytes (alt_result, 16, &ctx);

		/* Add salt for numbers not divisible by 3.  */
		if (cnt % 3 != 0)
			md5_process_bytes (salt, salt_len, &ctx);

		/* Add key for numbers not divisible by 7.  */
		if (cnt % 7 != 0)
			md5_process_bytes (key, key_len, &ctx);

		/* Add key or last result.  */
		if ((cnt & 1) != 0)
			md5_process_bytes (alt_result, 16, &ctx);
		else
			md5_process_bytes (key, key_len, &ctx);

		/* Create intermediate result.  */
		md5_finish_ctx (&ctx, alt_result);
	}

	/* Now we can construct the result string.  It consists of three
	parts.  */
	//memcpy (buffer, md5_salt_prefix, MAX (0, buflen));
	//cp = buffer;
	//buflen -= sizeof (md5_salt_prefix) - 1;

	//memcpy (cp, salt, MIN ((size_t) MAX (0, buflen), salt_len));
	//cp = buffer;
	//buflen -= MIN ((size_t) MAX (0, buflen), salt_len);

	//if (buflen > 0)
	//{
	//	*cp++ = '$';
	//	--buflen;
	//}

	//b64_from_24bit (cp, buflen, alt_result[0], alt_result[6], alt_result[12], 4);
	//b64_from_24bit (cp, buflen, alt_result[1], alt_result[7], alt_result[13], 4);
	//b64_from_24bit (cp, buflen, alt_result[2], alt_result[8], alt_result[14], 4);
	//b64_from_24bit (cp, buflen, alt_result[3], alt_result[9], alt_result[15], 4);
	//b64_from_24bit (cp, buflen, alt_result[4], alt_result[10], alt_result[5], 4);
	//b64_from_24bit (cp, buflen, 0, 0, alt_result[11], 2);
	//if (buflen <= 0)
	//{
	//	buffer = NULL;
	//}
	//else
	//	*cp = '\0';		/* Terminate the string.  */
}

//char * __md5_crypt (const char *key, const char *salt)
//{
//	/* We don't want to have an arbitrary limit in the size of the
//	password.  We can compute the size of the result in advance and
//	so we can prepare the buffer we pass to `md5_crypt_r'.  */
//	static int buflen;
//	int needed = 3 + strlen (salt) + 1 + 26 + 1;
//
//	if (buflen < needed)
//	{
//		char *new_buffer = (char *) realloc (buffer, needed);
//		if (new_buffer == NULL)
//			return NULL;
//
//		buffer = new_buffer;
//		buflen = needed;
//	}
//
//	return __md5_crypt_r (key, salt, buffer, buflen);
//}
//
//char* crypt (const char *key, const char *salt)
//{
//	// only concern about crypted by md5
//	return __md5_crypt (key, salt);
//}