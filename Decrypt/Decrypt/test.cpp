
#include "crypt.h"
#include "stdlib.h"
#include "string.h"
#include <stdio.h>
#include <map>
#include <windows.h>
#include <algorithm>
#include <array>
#include <amp.h>
#include <string>
#include "genpwd.h"

//#define GPU_BOOST

using namespace std;
using namespace concurrency;

//#define GPU_DEBUG
#define SET_RAW(raw, source, p0, p1, p2) \
	raw[p2] = source & 0xff; \
	raw[p1] = (source & 0xff00)>>8; \
	raw[p0] = (source & 0xff0000)>>16

void reconstructHash(char* source, unsigned int* result)
{
	static map<char, int>::value_type base64Data[] = {
		pair<char, int>('.', 0),		pair<char, int>('/', 1),		pair<char, int>('0', 2),		pair<char, int>('1', 3),
		pair<char, int>('2', 4),		pair<char, int>('3', 5),		pair<char, int>('4', 6),		pair<char, int>('5', 7),
		pair<char, int>('6', 8),		pair<char, int>('7', 9),		pair<char, int>('8', 10),		pair<char, int>('9', 11),
		pair<char, int>('A', 12),		pair<char, int>('B', 13),		pair<char, int>('C', 14),		pair<char, int>('D', 15),
		pair<char, int>('E', 16),		pair<char, int>('F', 17),		pair<char, int>('G', 18),		pair<char, int>('H', 19),
		pair<char, int>('I', 20),		pair<char, int>('J', 21),		pair<char, int>('K', 22),		pair<char, int>('L', 23),
		pair<char, int>('M', 24),		pair<char, int>('N', 25),		pair<char, int>('O', 26),		pair<char, int>('P', 27),
		pair<char, int>('Q', 28),		pair<char, int>('R', 29),		pair<char, int>('S', 30),		pair<char, int>('T', 31),
		pair<char, int>('U', 32),		pair<char, int>('V', 33),		pair<char, int>('W', 34),		pair<char, int>('X', 35),
		pair<char, int>('Y', 36),		pair<char, int>('Z', 37),		pair<char, int>('a', 38),		pair<char, int>('b', 39),
		pair<char, int>('c', 40),		pair<char, int>('d', 41),		pair<char, int>('e', 42),		pair<char, int>('f', 43),
		pair<char, int>('g', 44),		pair<char, int>('h', 45),		pair<char, int>('i', 46),		pair<char, int>('j', 47),
		pair<char, int>('k', 48),		pair<char, int>('l', 49),		pair<char, int>('m', 50),		pair<char, int>('n', 51),
		pair<char, int>('o', 52),		pair<char, int>('p', 53),		pair<char, int>('q', 54),		pair<char, int>('r', 55),
		pair<char, int>('s', 56),		pair<char, int>('t', 57),		pair<char, int>('u', 58),		pair<char, int>('v', 59),
		pair<char, int>('w', 60),		pair<char, int>('x', 61),		pair<char, int>('y', 62),		pair<char, int>('z', 63)
	};
	static map<char, int> base64Map(base64Data, base64Data+64);
	char* rawMem = (char*)(result);
	int offset = 0;
	int i;
	for (i=0; i<20; i+=4, offset++)
	{
		int i0 = base64Map[source[i]];
		int i1 = base64Map[source[i+1]];
		int i2 = base64Map[source[i+2]];
		int i3 = base64Map[source[i+3]];
		int k = i0 + (i1<<6) + (i2<<12) + (i3<<18);
		if (offset < 4)
		{
			SET_RAW(rawMem, k, 0+offset, 6+offset, 12+offset);
		}
		else
		{
			SET_RAW(rawMem, k, 0+offset, 6+offset, 5);
		}

	}

	int i0 = base64Map[source[i]];
	int i1 = base64Map[source[i+1]];
	rawMem[11] = (i0 + (i1<<6)) & 0xff;
}

void b64_from_24bit (char*&cp, unsigned int b2, unsigned int b1, unsigned int b0, int n)
{
	static const char b64t[65] = "./0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
	unsigned int w = (b2 << 16) | (b1 << 8) | b0;
	while (n-- > 0)
	{
		*cp++ = b64t[w & 0x3f];
		w >>= 6;
	}
}
void constructHash(unsigned int* source, char* result)
{
	unsigned char* k = (unsigned char*)(source);
	b64_from_24bit (result,  k[0], k[6], k[12], 4);
	b64_from_24bit (result,  k[1], k[7], k[13], 4);
	b64_from_24bit (result,  k[2], k[8], k[14], 4);
	b64_from_24bit (result,  k[3], k[9], k[15], 4);
	b64_from_24bit (result,  k[4], k[10], k[5], 4);
	b64_from_24bit (result,  0, 0, k[11], 2);
}

#define THREAD 100000/*102400*/
int main()
{
	unsigned int targetData[4];
	//char targetCode[] = "G5zqAOOl0VwpcL2a7GjZE0";
	char targetCode[] = "jpx7tAjhJg4ryhGnys8Fl/";
	//char targetCode[] = "Aq.gGF9Y8NGGNZueIJv2p/";
	char resultStr[32];	
	reconstructHash(targetCode, targetData);
	//constructHash(targetData, resultStr);

	__int64 begin = GetTickCount();

#if MD5_LIB == LIB_CPU
	std::array<char*, THREAD> a;
	unsigned int result[4];

	char(*p)[16] = genpwd();
	for(int i=0; i<THREAD; i++)
	{
		a[i] = new char[16];
		strcpy(a[i], "aaaaaj");
	}

	parallel_for_each (a.begin(), a.end(), [&](char* str) {
		__md5_crypt_r(str, strlen(str), "ShuJian", result);
	});

	//constructHash(result, resultStr);
#endif

#if MD5_LIB == LIB_GPU
	int* inputData = new int[THREAD*4];
	int* resultData = new int[THREAD];

	int detectedData;
	int resultStringData[4];


	concurrency::array_view<int, 1> detected(1, &detectedData);
	concurrency::array_view<int, 2> input(THREAD, 4, inputData);
	concurrency::array_view<unsigned int, 1> target(4, targetData);
	concurrency::array_view<int, 1> resultString(4, resultStringData);
#ifdef GPU_DEBUG
	unsigned int debugData[4];
	concurrency::array_view<unsigned int, 1> debug(4, debugData);
	debug.discard_data();
#endif
	concurrency::array_view<int, 1> result(THREAD, resultData);
	result.discard_data();

	int first = 1;
	for (int loop=0; loop<10; loop++)
	{

		// this cost 0.7s when THREAD==200k, need optimization
		char(*p)[16];
		if (first == 1)
		{
			p = genpwd();
			for(int i=0; i<THREAD; i++)
			{
				strcpy((char*)input[i].data(), p[i]);
			}
			first = 0;
		}

#ifndef GPU_BOOST	
		parallel_for_each(result.extent, [=](index<1> i) restrict(amp) {
			int index = i[0];
			unsigned int res[4];
			int salt[2];
			// "ShuJian"
			//tile_static int key[4];
			//key[0] = input[index].data()[0];
			//key[1] = input[index].data()[1];
			//key[2] = input[index].data()[2];
			//key[3] = input[index].data()[3];

			int key[4];
			key[0] = input[index].data()[0];
			key[1] = input[index].data()[1];
			key[2] = input[index].data()[2];
			key[3] = input[index].data()[3];


			salt[0] = 0x4a756853;
			salt[1] = 0x006e6169;
			__md5_crypt_r(key, PASSWORD_LENGTH, salt, res);

			if (res[0] == target(0) && res[1] == target(1) && res[2] == target(2) && res[3] == target(3))
			{
				result[i] = 1;
				detected[0] = 1;
				resultString[0] = key[0];
				resultString[1] = key[1];
				resultString[2] = key[2];
				resultString[3] = key[3];
			}
			else
			{
				result[i] = 0;
			}
		});
#else
		try {
			parallel_for_each(result.extent.tile<64>(), [=](tiled_index<64> i) restrict(amp) {
				int globalIndex = i.global[0];
				unsigned int res[4];
				int salt[2];
				// "ShuJian"
				tile_static int key[64][4];
				int localIndex = i.local[0];
				key[localIndex][0] = input[globalIndex].data()[0];
				key[localIndex][1] = input[globalIndex].data()[1];
				key[localIndex][2] = input[globalIndex].data()[2];
				key[localIndex][3] = input[globalIndex].data()[3];
				salt[0] = 0x4a756853;
				salt[1] = 0x006e6169;
				__md5_crypt_r(key[localIndex], 9, salt, res);
				//	i.barrier.wait();

				if (res[0] == target(0) && res[1] == target(1) && res[2] == target(2) && res[3] == target(3))
				{
					result[i] = 1;
					detected[0] = 1;
					resultString[0] = key[localIndex][0];
					resultString[1] = key[localIndex][1];
					resultString[2] = key[localIndex][2];
					resultString[3] = key[localIndex][3];
				}
				else
				{
					result[i] = 0;
				}
			});
		}
		catch(runtime_exception& ex)
		{
			printf("%s\n", ex.what());
		}
#endif
		if (first == 0)
		{
			p = genpwd();
			for(int i=0; i<THREAD; i++)
			{
				strcpy((char*)input[i].data(), p[i]);
			}
		}
		result.synchronize();
#ifdef GPU_DEBUG
		debug.synchronize();
		printf("%s\n", debugData);
#endif
		if (detected[0] == 1)
		{
			resultString.synchronize();
			printf("We got it!\n");
			printf("%s\n", resultStringData);
			getchar();
		}
	}
#endif

	printf("We can't find it\n");
	__int64 end = GetTickCount();
	printf("Elapse:%fs\n", (end-begin)/1000.0f);
	printf("Press any key to continue\n");
	getchar();
	return 0;
}